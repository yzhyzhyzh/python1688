#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import re

def get_page():          #获取不进行js解析的网页源代码并将其写入文件中
    url = "https://wangchujiang.com/linux-command/c/iptables.html"
    http = requests.get(url)
    print(http.text)
    with open('xxx.html','w',encoding='utf-8') as  f:
        f.write(http.text)

def test_beautiful_soup():  #用标签进行匹配的时候直接打标签名就可以，除此之外还可以用css选择器进行选择，来获取他的属性值，还有父节点和子节点的
                            #选取，不过注意迭代器类型要用for循环来得到他的值，或者调用ele...（忘记了）方法来得到迭代器的值。
    with open('test.html','r',encoding='utf-8') as f:
        html=f.read()
        #print(html)
        soup = BeautifulSoup(html,'lxml')
        #print(type(soup))
        # contents = soup.find_all(content=True)     #迭代器的查询
        # for content in contents:
        #     print(content)
        # content2 = soup.text('text')      #css选择器（感觉这个要容易一点来匹配）
        # print(content2)
        #name = soup.span.a['href']   #基本用标签来获取所需要的信息，以及标签中的属性（['属性名']），文字（.string方法来获取，嵌套关系（tagname.tagname...以此类推））
        #print(name)
        ul = soup.select('ul li')
        for u in ul:
            u = str(u)
            u = re.findall(r'<a href="#(.*?)">',u,re.S)
            print(u)

def beautiful_soup_discendant():
    with open('test.html','r',encoding='utf-8') as f:
        html = f.read()
        soup = BeautifulSoup(html,'lxml')
        sons = soup.head.descendants
        for i,son in enumerate(sons):
            #print(i,son)             #descendants  选取子孙节点的方法 enumerate用来循环迭代器中的元素和他的索引。python真好用，skr
                                      #同理，会有.parent(输出父节点)，.parents(输出祖先节点)。.next_siblings和previous_siblings
                                      #(分别输出当前节点之后和之前的兄弟节点。)
            son = str(son)
            chinese = re.findall(r'[\u4e00-\u9fa5]',son,re.S)
            print(chinese)



def beautiful_soup_biaozhunseletor():             #之前都是标签选择器，此函数实现的是标准选择器
    with open('test.html','r',encoding='utf-8') as f:
        html = f.read()
        soup = BeautifulSoup(html,'lxml')
        ul = soup.find_all('ul')
        for u in ul:
            li = u.find_all('li')
            #print(li)
            for l in li:
                a = l.find_all('a')
                eg = re.findall(r'"#.*?">',str(a))  #用正则来匹配href中的内容
                for e in eg:
                    e = e.lstrip('"#')
                    e = e.rstrip('">')
                    print(e)                        #清洗数据

def main():
    beautiful_soup_biaozhunseletor()

if __name__ == '__main__':
    main()
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from importlib import reload
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import  expected_conditions as EC
from pyquery import PyQuery as PQ
import pandas as pd
import csv
import  time
import re
import io
import sys
reload(sys)
#sys.setdefaultencoding("utf-8")
browser = webdriver.Chrome()
wait = WebDriverWait(browser,10)
products = []
headers = ['image','name','price','total_price','addr']
def search():
    browser.get('https://www.1688.com/')
    buttom = browser.find_element_by_css_selector('#j-identity > div > span > i')
    buttom.click()
    input1 = browser.find_element_by_id('alisearch-keywords')
    input1.send_keys('笔记本')
    submit = browser.find_element_by_css_selector('#alisearch-submit')
    submit.click()
    buttom_1 = browser.find_element_by_css_selector('#s-module-overlay > div.s-overlay-layer > div > div.s-overlay-close > em.s-overlay-close-l')
    buttom_1.click()

def next_page(page_number):
    input = browser.find_element_by_css_selector('#fui_widget_4 > div > span.fui-number > input')
    input.clear()
    input.send_keys(page_number)
    buttom = browser.find_element_by_css_selector('#fui_widget_4 > div > span.fui-forward > button')
    buttom.click()
def main():
    search()
    time.sleep(2)
    browser.execute_script("window.scrollTo(0,document.body.scrollHeight)")
    total = browser.find_element_by_css_selector('#fui_widget_4 > div > span.fui-paging-total > em')
    total = int(total.text)
    print(total)
    for i in range(2,10):
        next_page(i)
        time.sleep(3)
        get_products()



def get_products():
    #wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR,'#sm-maindata > div')))
    html = browser.page_source
    #print(html)
    html_1 = re.findall(r'<div class="sm-offer ">.*?</div></div>',html,re.S)[0]
    #print(html_1)
    html_2 = PQ(html_1)
    #print(html_2('li'))
    items = html_2('li').items()
    for item in items:
        #print(item)

        product = {
            'image': item.find('.imgofferresult-mainBlock .sm-offer-photo a img').attr('src'),
            'name': item.find('.imgofferresult-mainBlock a img').attr('alt'),
            'price': item.find('.imgofferresult-mainBlock span').attr('title'),
            'total_price': item.find('.imgofferresult-mainBlock .sm-offer-tradeBt').attr('title'),
            'addr': item.find('.imgofferresult-mainBlock .sm-offer-companyName').attr('title')

        }
        if product['name'] == None:
            continue


        products.append(product)
    print(products)
    with open('product.csv','w',encoding=' gb18030') as f:

        f_csv = csv.DictWriter(f, headers)
        f_csv.writeheader()
        f_csv.writerows(products)



if __name__ == '__main__':
    main()
    browser.close()

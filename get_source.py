# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     get_source
   Description :
   Author :       yzh
   date：          2022/10/29
-------------------------------------------------
   Change Activity:
                   2022/10/29:
-------------------------------------------------
"""
__author__ = 'yzh'
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ChromeOptions
import pyquery as PQ
import pymysql
import re,sys,time


def get_zy_news(url):
    """
    爬取中央网站的新闻标题和时间信息,传入要爬取统战部新闻网址的url
    :return: 函数返回两个list，新闻时间news_date_list以及新闻标题news_title_list
    """
    path = 'D:\code\\bigdatacenter-master\\bigdatacenter-master\\chromedriver\\chromedriver.exe'
    # 创建浏览器操纵对象且设置浏览器不加载图片

    option = ChromeOptions()
    prefs = {"profile.managed_default_content_settings.images":2}
    option.add_experimental_option("prefs", prefs)
    browser = webdriver.Chrome(path, chrome_options=option)
    # 访问网站
    # url = 'http://www.zytzb.gov.cn/tzsx/index.jhtml'



    browser.get(url)

    news_date_list = []
    news_title_list = []
    news_mediasource_list = []

    content = browser.find_elements(By.XPATH,'/html/body/div[2]/div[2]/div/div[1]/ul')
    print(content)
    for content_list in content:
        get_html1 = content_list.get_attribute('innerHTML')
    print(get_html1)
    print('=======================================start=============================')
    date_list = re.findall(r'<span class="date">.*?</span>', get_html1, re.S)
    title_list = re.findall(r'<a target=.*?title=.*?">.*?</a>',get_html1,re.S)
    herf_list = re.findall(r'<a target="_blank" href=".*?" title',get_html1,re.S)

    print("==============date-list===================")
    for date_result in date_list:
        date_result = re.findall(r'>(.*?)</span>',date_result,re.S)
        # print(type(date_result[0]))
        news_date_list.append(date_result[0].strip("'"))
        #print(date_result)

    print("===============title-list==================")
    for title_result in title_list:
        title_result = re.findall(r'>(.*?)</a>',title_result,re.S)
        news_title_list.append(title_result[0])

    print("===============herf-list===================")
    for herf_result in herf_list:
        herf_result = re.findall(r'href="(.*?)" title', herf_result, re.S)
        herf_result = "http://www.zytzb.gov.cn/" + herf_result[0]
        # print(herf_result)
        browser.get(herf_result)
        time.sleep(1)
        content = browser.find_elements(By.XPATH, '/html/body/div[2]/div[2]/div[2]/div[2]')
        if len(content) == 0:
            news_mediasource_list.append("网页出错")
        else:
            for content_list in content:
                media_source_html = content_list.get_attribute('innerHTML')
                # print(media_source_html)
                # print(type(media_source_html))
                media_source = media_source_html.lstrip("来源：")
                # print(media_source)
                news_mediasource_list.append(media_source)

        #print(title_result)
    print("=================date-list================================")
    print(len(news_date_list))
    print("******************title-list********************************")
    print(len(news_title_list))
    print("==================get_source中的媒体来源列表=======================")
    print(len(news_mediasource_list))

    # date = date_list('span').
        # print("---------------------------------------------------")
        # print(content_list.get_attribute('innerHTML'))
        # print(a+1)
        # print(type(content_list.text))
        # print("---------------------------------------------------")

    browser.close()
    return news_date_list,news_title_list,news_mediasource_list

def get_n_page(n_of_pages):
    """

    :param n_of_pages:要爬取的信息的页数
    :return: 返回新闻时间和标题的列表
    """
    news_date_list = []
    news_title_list = []
    news_mediasource_list = []
    if n_of_pages == 1:
        url = 'https://www.zytzb.gov.cn/zytzb/index/tzsx/index.shtml'
        # 注意这个url可能会有变化，要提前访问下看存不存在
        news_date_list,news_title_list,news_mediasource_list = get_zy_news(url)
    else:
        for i in range(n_of_pages):
            # time.sleep(1) # 防止页面加载太快被封
            url = 'https://www.zytzb.gov.cn/zytzb/index/tzsx/e85c86fb-'+str(i+1)+'.shtml'
            # 同上面的url，这个可能会变
            temp_news_date_list,temp_news_title_list,temp_news_mediasource_list = get_zy_news(url)
            news_date_list.extend([x for x in temp_news_date_list[0:]])
            news_title_list.extend([x for x in temp_news_title_list[0:]])
            news_mediasource_list.extend([x for x in temp_news_mediasource_list[0:]])

    # 进行标题数据筛选
    delete_title_index_list = []
    delete_title_index = 0
    for i in news_title_list:
        i_temp = re.match(r'.*?台.*?|.*?党.*?|.*?两岸.*?|.*?京.*?|.*?二十大.*?', i, re.S)
        if i_temp == None:
            print("正在进行标题筛选")
            # print(news_title_list.index(i))
            delete_title_index_list.append(delete_title_index)
        delete_title_index += 1
    delete_title_index_list = delete_title_index_list[::-1]
    for delete_index in delete_title_index_list:
        news_title_list.pop(delete_index)
        news_date_list.pop(delete_index)
        news_mediasource_list.pop(delete_index)
    print("==========输出进行标题筛选过后的列表===========")
    print(news_title_list)
    print("上表长度为" + str(len(news_title_list)))
    print(news_date_list)
    print("上表长度为" + str(len(news_date_list)))
    print(news_mediasource_list)
    print("上表长度为" + str(len(news_mediasource_list)))
    print("===============================================")
    # 进行媒体信息筛选
    delete_media_index_list = []
    delete_media_index = 0
    for i in news_mediasource_list:

        print("正在进行媒体筛选")
        title = news_title_list[delete_media_index]
        title_temp = re.match(r'.*?二十大.*?',title,re.S)
        if title_temp != None:
            continue
        i_temp = re.search(r'^中国和平统一促进会网站.*?|^中国新闻网|^中央社院.*?|^中新.*?|^全国台联网站|^台胞之家.*?',i,re.S)
        if i_temp == None:
            delete_media_index_list.append(delete_media_index)
        delete_media_index += 1

    delete_media_index_list = delete_media_index_list[::-1]
    for delete_index in delete_media_index_list:
        news_title_list.pop(delete_index)
        news_date_list.pop(delete_index)
        news_mediasource_list.pop(delete_index)

    # 进行网页二次筛选，选出不符合正则匹配的删掉
    delete_media_index_list_2 = []
    delete_media_index = 0
    for i in news_mediasource_list:
        print("正在进行媒体筛选第2遍")
        i_temp = re.search(r'.*?藏.*?|.*?网页出错.*?|.*?职业.*?',i,re.S)
        print(i_temp)
        if i_temp != None:
            delete_media_index_list_2.append(delete_media_index)
        delete_media_index += 1
    print(delete_media_index_list_2)
    print(len(delete_media_index_list_2))
    delete_media_index_list_2 = delete_media_index_list_2[::-1]
    for delete_index_2 in delete_media_index_list_2:
        news_title_list.pop(delete_index_2)
        news_date_list.pop(delete_index_2)
        news_mediasource_list.pop(delete_index_2)
    #
    # delete_media_index_list_3 = []
    # for i in news_mediasource_list:
    #     print("正在进行媒体筛选第2遍")
    #     i_temp = re.search(r'.*?藏.*?|.*?网页出错.*?|.*?职业.*?', i, re.S)
    #     print(i_temp)
    #     if i_temp != None:
    #         delete_media_index_list_3.append(news_mediasource_list.index(i))
    # print(delete_media_index_list_3)
    # print(len(delete_media_index_list_3))
    # delete_media_index_list_3 = delete_media_index_list_3[::-1]
    # for delete_index_3 in delete_media_index_list_3:
    #     news_title_list.pop(delete_index_3)
    #     news_date_list.pop(delete_index_3)
    #     news_mediasource_list.pop(delete_index_3)


    return news_date_list,news_title_list,news_mediasource_list

# 对本函数进行测试的代码如下
'''
get_n_page(num_of_pages)
'''

# get_n_page(3)
get_zy_news('https://www.zytzb.gov.cn/zytzb/index/tzsx/e85c86fb-1.shtml')


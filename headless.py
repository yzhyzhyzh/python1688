#!/usr/bin/env python
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

chrome_options = Options()                                  #浏览器的初始化操作
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
dirver = webdriver.Chrome(chrome_options=chrome_options)
def search():                                               #页面的初始加载
    dirver.get('http://www.1688.com')
    print(dirver.page_source)
    time.sleep(5)
    buttom = dirver.find_element_by_css_selector('#j-identity > div > span > i')
    buttom.click()
    submit = dirver.find_element_by_css_selector('#alisearch-keywords')
    submit.send_keys('自行车')
    buttom_3 = dirver.find_element_by_css_selector('#alisearch-submit')
    buttom_3.click()
# with open('1688.html','w',encoding='utf-8') as f:
#      f.write(dirver.page_source)
    time.sleep(3)
    buttom_1 = dirver.find_element_by_css_selector('#s-module-overlay > div.s-overlay-layer > div > div.s-overlay-close > em.s-overlay-close-l')
    buttom_1.click()
    print(dirver.page_source)
# if dirver.find_element_by_css_selector('#fui_widget_4 > div > span.fui-forward > button'):
def next_page():                                            #进行页面的翻页操作
    print('页面加载成功，即将进行翻页操作')
    for i in range(2,4):                                    #1688的页面大概只有50页，所以页面是没必要到100页的
        input = dirver.find_element_by_css_selector('#fui_widget_4 > div > span.fui-number > input')
        input.clear()
        input.send_keys(i)
        buttom_2 = dirver.find_element_by_css_selector('#fui_widget_4 > div > span.fui-forward > button')
        buttom_2.click()
        print('当前页面在第'+ str(i) + '页')
def main():                                                 #主函数有点怪。
    search()
    print('search is done!')
    next_page()
    dirver.close()

if __name__ == '__init__':
    main()



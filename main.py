# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     main
   Description :
   Author :       yzh
   date：          2022/10/31
-------------------------------------------------
   Change Activity:
                   2022/10/31:
-------------------------------------------------
"""
__author__ = 'yzh'

def main():
    import sys
    sys.path.append(r'D:\Big_Data_Center_\tongzhanbu\venv\selenium_tzb')
    import get_source


    news_date_list = []
    news_title_list = []
    news_mediasource_list = []
    news_date_list,news_title_list,news_mediasource_list = get_source.get_n_page(5)
    print(news_date_list)
    print(len(news_date_list))
    print(news_title_list)
    print(len(news_title_list))
    print(news_mediasource_list)
    print(len(news_mediasource_list))


    # 入库操作
    import pymysql

    db = pymysql.connect(
        host='100.89.6.10',
        port=3306,
        user='crawl_user',
        password='crawlUser@2022',
        database='crawl_db'
    )

    cursor = db.cursor()
    for list_num in range(len(news_date_list)):
        sql = "INSERT INTO `bd_data`.`china_govmt_mov_news` (`prov_name`, `city_name`, `area_name`, `media_name`, `news_title`, `issu_time`, `create_person`)" \
              " VALUES ('0', '0', '0', '%s', '%s', '%s', 'yzh')" %(news_mediasource_list[list_num].replace("网站","网"),news_title_list[list_num], news_date_list[list_num])
        cursor.execute(sql)
    db.commit()
    print("============插入成功================")


    # result = cursor.fetchall()
    # for data in result:
    #     print(data)


if __name__ == '__main__':
    main()